<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TenantController;
use App\Http\Controllers\Tenant\TenantProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     // return view('welcome');
//     return view('form');
// })->name('home');
// Route::middleware(['tenantmiddleware:6'])->get('/product', [TenantProductController::class, 'product']);

// Route::post('/create-tenant', [TenantController::class, 'create_tenant']);