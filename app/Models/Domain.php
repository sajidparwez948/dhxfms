<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Stancl\Tenancy\Database\Concerns\HasDomains;
use Stancl\Tenancy\Database\Concerns\HasDatabase;

class Domain extends Model
{
    use HasFactory, HasDomains, HasDatabase;

    public function tenant()
    {
        return $this->belongsTo(Tenant::class, 'tenant_id');
    }
}