<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $attributes = [
        'IsActive' => '1',
    ];

    protected $appends = ['name'];

    /**
     * A user may be assigned many roles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * Assign a new role to the user.
     *
     * @param  mixed  $role
     */


    /**
     * Fetch the user's abilities.
     *
     * @return array
     */


    /**
     * Get the user's full name.
     *
     * @return string
     */
    public function getNameAttribute()
    {
        return ucwords("{$this->first_name} {$this->last_name}");
    }
    public function setNameAttribute($name)
    {
        $name = explode(" ", $name);

        if (count($name) > 1) {
            $this->attributes['first_name'] =  $name[0];
            $this->attributes['last_name'] =  $name[1];
        } else {
            $this->attributes['first_name'] =  $name[0];
            $this->attributes['last_name'] =  '';
        }
    }

    // public function smtp()
    // {
    //     return $this->hasOne(Smtp::class);
    // }
    // public function emailData()
    // {
    //     return $this->hasMany(EmailData::class);
    // }
}