<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Http\Requests\NovaRequest;
use Stancl\Tenancy\Database\Concerns\HasDomains;
use Stancl\Tenancy\Database\Concerns\HasDatabase;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\PasswordConfirmation;
use Laravel\Nova\Fields\Email;
use \App\Models\Tenants\User;


class Tenant extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */


    public static $model = \App\Models\Tenant::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        // $table = self::$model::findOrFail(request()->resourceId);
        // prx(request()->all());
        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make(__('Tenant Name'), 'tenant_name')->creationRules('unique:tenants,data->tenant_name'),
            // ->fillUsing(function ($request, $model, $attribute, $requestAttribute) {
            //     $model->{$attribute} = $request->input($attribute);
            // }),
            // Text::make('Database name')->displayUsing(function ($name) use ($table) {

            //     // return strtoupper($name);
            // }),
            // Text::make(__('User Name'), 'name')->hideWhenUpdating(),
            // Text::make(__('email'), 'email')->hideWhenUpdating(),

            // Password::make('Password')
            //     ->onlyOnForms()
            //     ->creationRules('required', 'string', 'min:8', 'confirmed')
            //     ->updateRules('nullable', 'string', 'min:8', 'confirmed')->hideWhenUpdating(),

            // PasswordConfirmation::make('Password Confirmation')->hideWhenUpdating(),

            HasMany::make('domains'),
        ];
    }

    // public static function afterCreate(NovaRequest $request, $model)
    // {
    //     prx("hi");
    // }

    // public static function afterUpdate(NovaRequest $request, $model)
    // {
    //     prx("hi");
    // }

    // public static function redirectAfterCreate(NovaRequest $request, $resource)
    // {
    //     // prx($request);
    //     // prx(tenant($resource->id));
    //     // $user = new  User;

    //     // $user->name  = $resource->name;
    //     // $user->email = $resource->email;
    //     // $user->password = $resource->password;
    //     // $user->save();

    //     return '/resources/' . static::uriKey() . '/' . $resource->getKey();
    // }

    // public static function redirectAfterUpdate(NovaRequest $request, $resource)
    // {
    //     // return '/resources/' . static::uriKey() . '/' . $resource->getKey();
    // }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}