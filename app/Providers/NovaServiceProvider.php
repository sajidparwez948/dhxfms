<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Laravel\Nova\Cards\Help;
use Laravel\Nova\Nova;
use Laravel\Nova\NovaApplicationServiceProvider;
use Stancl\Tenancy\Middleware\InitializeTenancyByDomain;
use Stancl\Tenancy\Middleware\PreventAccessFromCentralDomains;
use Stancl\Tenancy\Middleware\InitializeTenancyByDomainOrSubdomain;
use Dhxfms\Tenant\Tenant;
use Dhxfms\Central\Central;
use Illuminate\Support\Str;

class NovaServiceProvider extends NovaApplicationServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        // Nova::createUserUsing(function ($command) {
        //     return [
        //         $command->ask('Name'),
        //         $command->ask('Email Address'),
        //         $command->secret('Password'),
        //     ];
        // }, function ($name, $email, $password) {
        //     (new \User)->forceFill([
        //         'name' => $name,
        //         'email' => $email,
        //         'password' => Hash::make($password),
        //         'email_verified_at' => now(),
        //         'activated_at' => now(), // my custom field
        //     ])->save();
        // });
    }

    /**
     * Register the Nova routes.
     *
     * @return void
     */
    protected function routes()
    {
        Nova::routes()
            // ->withAuthenticationRoutes()
            // ->withPasswordResetRoutes()
            ->withAuthenticationRoutes([
                // You can make this simpler by creating a tenancy route group
                InitializeTenancyByDomainOrSubdomain::class,
                // PreventAccessFromCentralDomains::class,
                'web',
                'universal',


            ])
            ->withPasswordResetRoutes([
                // You can make this simpler by creating a tenancy route group
                InitializeTenancyByDomainOrSubdomain::class,
                // PreventAccessFromCentralDomains::class,
                'web',
                'universal',

            ])
            ->register();
    }

    /**
     * Register the Nova gate.
     *
     * This gate determines who can access Nova in non-local environments.
     *
     * @return void
     */
    protected function gate()
    {
        Gate::define('viewNova', function ($user) {
            return in_array($user->email, [
                //
            ]);
        });
    }

    /**
     * Get the cards that should be displayed on the default Nova dashboard.
     *
     * @return array
     */
    protected function cards()
    {
        return [
            new Help,
        ];
    }

    /**
     * Get the extra dashboards that should be displayed on the Nova dashboard.
     *
     * @return array
     */
    protected function dashboards()
    {
        return [];
    }

    /**
     * Get the tools that should be listed in the Nova sidebar.
     *
     * @return array
     */
    public function tools()
    {
        if (tenant()) {
            return [
                (new Tenant)->canSee(function ($request) {
                    return true;
                }),
            ];
        } else {
            return [
                (new Central)->canSee(function ($request) {
                    return true;
                }),
            ];
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Nova::initialPath('resources/users');
    }

    protected function resources()
    {

        if (tenancy()->initialized) {
            Nova::resourcesIn(app_path('NovaTenant'));
        } else {
            if (!in_array(request()->getHost(), config('tenancy.central_domains'))) {
                abort(404);
            }
            Nova::resourcesIn(app_path('Nova'));
        }
    }
}