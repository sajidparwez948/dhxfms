<?php

namespace App\NovaTenant;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Textarea;
use Laravel\Nova\Http\Requests\NovaRequest;
use App\Nova\Resource;
use DigitalCreative\ConditionalContainer\ConditionalContainer;
use DigitalCreative\ConditionalContainer\HasConditionalContainer;
use Laravel\Nova\Fields\Heading;

class Customer extends Resource
{

    use HasConditionalContainer;
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Tenants\Customer::class;

    // public static $group = "";

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'id';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {

        return [
            ID::make(__('ID'), 'id')->sortable(),
            Text::make(__('Customer Name'), 'customer_name')->rules('required'),
            Text::make(__('Customer Type'), 'customer_type'),
            Text::make(__('Contact Person Name'), 'contact_person_name')->rules('required'),
            Text::make(__('Contact Person Phone'), 'contact_person_phone')->rules('required'),
            Text::make(__('Contact Person Email'), 'contact_person_email')->rules('required'),
            Textarea::make(__('Mailing Address'), 'mailing_address')->rules('required'),
            Textarea::make(__('Billing Address'), 'billing_address'),
            Text::make(__('Radius'), 'geo_radius'),
            Text::make(__('Description'), 'description'),
            Text::make(__('Customer Id'), 'customer_id'),



            // ConditionalContainer::make([
            //     Heading::make('<p class="text-danger">Please write a good reason...</p>')->asHtml()
            // ])->if('statt truthy false'),



        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}