<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invoice\InvoiceFactory;

class InvoiceController extends Controller
{
    public function getInvoice()
    {
        $type = auth()->user()->load('role')->role->label ?? null;

        $factory = new InvoiceFactory;
        $invoice = $factory->createInvoice(strtolower($type));
        $result = $invoice->invoice();

        if ($result) {
            return response()->json(['success' => true, 'invoice' => $result]);
        } else {
            return response()->json(['success' => false, 'invoice' => null]);
        }
    }
}