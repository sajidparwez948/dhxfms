<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tenant;
use Illuminate\Support\Str;

class TenantController extends Controller
{
    public function create_tenant()
    {;

        $domain = null;

        try {
            \DB::transaction(function () use (&$domain) {

                $tenant = Tenant::create(['id' => request('name')]);
                $tenant->domains()->create(['domain' => request('domain')]);
                if (!Str::contains(request('domain'), '.')) {
                    $domain = request('domain') . '.' . config('app.url');
                } else {
                    $domain = request('domain');
                }
            });
            if ($domain) {
                return redirect(tenant_route($domain, 'home'));
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }
}