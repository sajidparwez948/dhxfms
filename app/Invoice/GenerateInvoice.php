<?php

namespace App\Invoice;

interface GenerateInvoice
{
    public function invoice();
}