<?php

namespace App\Invoice;

use App\Invoice\GenerateInvoice;

class GenerateVendorInvoice implements GenerateInvoice
{
    public function invoice()
    {
        return "Vendor invoice";
    }
}