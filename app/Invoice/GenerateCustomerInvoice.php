<?php

namespace App\Invoice;

use App\Invoice\GenerateInvoice;

class GenerateCustomerInvoice implements GenerateInvoice
{
    public function invoice()
    {
        return "Customer invoice";
    }
}