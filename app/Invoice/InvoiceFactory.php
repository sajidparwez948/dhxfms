<?php

namespace App\Invoice;

use App\Invoice\GenerateVendorInvoice;
use App\Invoice\GenerateCustomerInvoice;

class InvoiceFactory
{
    public static function createInvoice($invoice_type)
    {
        if (!$invoice_type)
            return null;
        switch ($invoice_type) {
            case "customer":
                return new GenerateCustomerInvoice();
            case "vendor":
                return new GenerateVendorInvoice();
            default:
                throw new \Exception("Unknown channel " . $invoice_type);
        }
    }
}