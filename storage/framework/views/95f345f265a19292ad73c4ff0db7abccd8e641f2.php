<?php if(count(\Laravel\Nova\Nova::resourcesForNavigation(request()))): ?>
    

    <?php $__currentLoopData = $navigation; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $group => $resources): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <?php if(count($groups) > 1): ?>
            <h4 class="ml-8 mb-4 text-xs text-white-50% uppercase tracking-wide"><?php echo e($group); ?></h4>
        <?php endif; ?>

        <ul class="list-reset mb-8">
            <?php $__currentLoopData = $resources; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $resource): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li class="leading-tight mb-4 ml-8 text-sm">
                    <router-link :to="{
                        name: 'index',
                        params: {
                            resourceName: '<?php echo e($resource::uriKey()); ?>'
                        }
                    }" class="text-white text-justify no-underline dim" dusk="<?php echo e($resource::uriKey()); ?>-resource-link">
                        <?php echo e($resource::label()); ?>

                    </router-link>
                </li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php endif; ?>
<?php /**PATH C:\Users\sajid\Desktop\dhxfms\resources\views/vendor/nova/resources/navigation.blade.php ENDPATH**/ ?>