<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            ['name' => 'admin', 'label' => 'Admin'],
            ['name' => 'cs_rep', 'label' => 'CS Rep'],
            ['name' => 'trade_show', 'label' => 'Trade Show'],
            ['name' => 'customer', 'label' => 'Customer'],
            ['name' => 'vendor', 'label' => 'Vendor'],
            ['name' => 'dhx', 'label' => 'dhx']
        ]);
    }
}