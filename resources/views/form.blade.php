<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <meta name="author" content="">
    <link rel="stylesheet" href="/css/app.css">
    <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">


  <!-- Custom styles for this template -->
   <link href="/css/album.css" rel="stylesheet">
  </head>
  <body class="m-2">
    <form class="m-2 p-2 flex" method="post" action="{{url('/create-tenant')}}">
      @csrf
      <div class="form-group">
        <label for="name">Tenant name</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Enter id">
      
      </div>
      <div class="form-group">
        <label for="domain">doamin  for your website</label>
        <input type="text" class="form-control" id="domain" name="domain" placeholder="domain">
      </div>
      
      <button type="submit" class="btn btn-primary">create</button>
    </form>
  </body>


  </html>
